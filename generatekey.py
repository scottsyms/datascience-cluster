#!/usr/local/bin/python3

import paramiko
import os
import datetime


template=open('cloud-config.template', 'r').read()
key=paramiko.RSAKey.generate(2048)
try:
    os.mkdir("./keys")
except:
    pass
key.write_private_key_file('./keys/private-' + datetime.datetime.now().strftime("%y%m%d%H%M%S") + '.key')
pub=template.replace("REPLACEME", "ssh-rsa " + key.get_base64())
print(pub, file=open('cloud-config.yaml', 'w'))
