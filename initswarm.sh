#!/bin/sh
ansible manager -i inventory.yaml  --key-file "keys/private-210622195045.key" -u ubuntu -a "docker swarm init"
JOINSTRING=`ansible manager -i inventory.yaml  --key-file "keys/private-210622195045.key" -u ubuntu -a "docker swarm join-token manager"`
COMMAND=`echo $JOINSTRING|sed 's/:/\n/g'|grep docker`
echo $COMMAND
ansible worker -i inventory.yaml  --key-file "keys/private-210622195045.key" -u ubuntu -a "$COMMAND"
