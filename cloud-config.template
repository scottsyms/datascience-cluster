# cloud-config

# Ensure the ubuntu user is a docker user
system_info:
  default_user:
    groups: docker
    ssh_authorized_keys:
      - REPLACEME

# Timezone
timezone: "etc/utc"


# Add docker repository to apt sources
apt:
  sources:
    docker.list:
      source: deb [arch=amd64] https://download.docker.com/linux/ubuntu $RELEASE stable
      keyid: 9DC858229FC7DD38854AE2D88D81803C0EBFCD88

# Install base packages
packages:
  - docker-ce
  - docker-ce-cli
  - ansible
  - screen
  - apt-transport-https
  - ca-certificates
  - curl
  - software-properties-common
  - virtualenv
  - python3-setuptools
  - git
  - python3-pip
  - build-essential
  - libssl-dev
  - libffi-dev
  - python3-dev
  - libldap2-dev

# Add localhost to the ansible hosts file
write_files:
  - path: /etc/ansible/hosts
    content: | 
       localhost
    append: true

# Install latest stable docker and docker-compose
runcmd:
    - [ sh, -c, 'sudo curl -L https://github.com/docker/compose/releases/download/$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep "tag_name" | cut -d \" -f4)/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose' ]
    - [ sh, -c, 'sudo chmod +x /usr/local/bin/docker-compose' ]
    - [ sh, -c, 'pip install --upgrade pip']
    - [ sh, -c, 'pip install dask[complete]']
    - [ sh, -c, 'pip install ipython']
    - [ sh, -c, 'pip install geopy']
    - [ sh, -c, 'pip install geopandas']
    - [ sh, -c, 'pip install fiona']
    - [ sh, -c, 'pip install vaex']
    - [ sh, -c, 'pip install polars']
    - [ sh, -c, 'mkdir /home/ubuntu/datascience-cluster']
    - [ sh, -c, 'git -C /home/ubuntu/datascience-cluster clone https://gitlab.com/scottsyms/datascience-cluster.git' ]