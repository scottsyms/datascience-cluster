Tag v0.1 creates standalone docker environment.
Tag v0.2 creates a docker swarm.

# datascience-cluster
The scripts were created to create a local cluster of virtual Ubuntu machines for data science experimentation.  The top level project uses Python and shell scripts to create a cluster of virtual machine, which can be further customised using the scripts in the project sub-directories.

The vm's are named with sequence numbers, for example, if you choose a root name of "python" the vm's will be labeled python1, python2, etc.

## Requirements
The scripts were created for a Mac, but should work under Linux as well.   You'll need Canonical's multipass and Python 3 with the Paramiko SSH library.  I recommend using Homebrew to install both Python and Multipass on the Mac.

## How to use
### Generate Certs
Run *generatekey.py* to create an SSH key pair.  The private key gets saved to *private.key* and its matching public key gets merged with the *cloud-config.template* file and written to *cloud-config.yaml*.

## Create VMs
Edit the shell script environment variables in *createmultipassvms.sh* to meet your needs and run the script.  The program will use multipass to create the required vms.

## Accessing the new VM's
There are two ways to do this- *multipass shell vmname* will bring you to the *ubuntu* user account on the vm, where it has sudo access.

You can also use SSH.  Run *multipass list* to get the IP address of the desired vm and run **ssh -i private.key ubuntu@xxx.xxx.xxx.xx*.

## References
Multipass documenation can be found at https://multipass.run/.

Cloud-init documentation: https://cloudinit.readthedocs.io/en/latest/

Homebrew documentation: https://brew.sh/

## To do
The networking on these images is not bridged, so they're only accessible on the host machine.  Moving the virtualiser from hyperkit to virtualbox should open up greater access to the vms.

Cloud-init can be leveraged with Hashicorp Packer and Terraform to supply Azure and AWS images.
