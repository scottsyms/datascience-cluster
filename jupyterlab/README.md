# Jupyter Lab Notebook
This is a slightly modified implementation of the official Jupyter Notebook docker image.

## Installing
Shell/SSH into the VM and check out the project with git- https://gitlab.com/scottsyms/datascience-cluster.git 

CD into the new directory and run *docker-compose up -d*.  The first run will take some time as it's pulling down the reference image from dockerhub and modifying it.

Exit the shell (control-d or Exit) and you can use *multipass list* to get the IP address of the image which you can use to access the notebook with the following URL.

http://IPaddress

If you're presented with a password challenge, use *password* .

## References
Jupyter notebook stacks: https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html

