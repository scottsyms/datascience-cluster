#!/bin/sh

VMS=4
MEM=1G
DISK=15G
CPUS=1
ROOTNAME=vm

create_vm () {
    # Create images
    multipass launch -d $DISK -m $MEM -c $CPUS -n $ROOTNAME$i --cloud-init cloud-config.yaml

    echo "Created: $ROOTNAME$i"
    }

for i in $(seq $VMS)
do
    create_vm
done

echo "[all]" > inventory.yaml
multipass list|grep Running|awk '{print $3}' >> inventory.yaml